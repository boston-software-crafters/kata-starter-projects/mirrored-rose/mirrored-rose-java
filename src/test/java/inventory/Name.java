package inventory;

/**
 * @author Danil Suits (danil@vast.com)
 */
public class Name {
    public static final String VEST = "+5 Dexterity Vest";
    public static final String ELIXIR = "Elixir of the Mongoose";
    public static final String BRIE = "Aged Brie";
    public static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
    public static final String TICKETS = "Backstage passes to a TAFKAL80ETC concert";
    public static final String CAKE = "Conjured Mana Cake";
}
