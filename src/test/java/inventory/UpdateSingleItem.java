package inventory;

import legacy.Item;

public class UpdateSingleItem {
    public static String scenario(String name, int sellIn, int quality) {
        Item item = new Item(name, sellIn, quality);

        Inventory.updateQuality(item);

        return item.toString();
    }

    public static void test(String name, int beforeSellIn, int beforeQuality,
                     int afterSellIn, int afterQuality) {
        Item control = item(name, afterSellIn, afterQuality);
        String expected = control.toString();
        String actual = UpdateSingleItem.scenario(name, beforeSellIn, beforeQuality);

        if ( ! expected.equals(actual)) {
            String scenario = item(name, beforeSellIn, beforeQuality).toString();
            throw new AssertionError(
                    "\t" + "Scenario: " + scenario
                            + "\n\t" + "Expected: " + expected
                            + "\n\t" + "  Actual: " + actual
            );
        }
    }

    static Item item(String name, int sellIn, int quality) {
        return new Item(name, sellIn, quality);
    }

}
