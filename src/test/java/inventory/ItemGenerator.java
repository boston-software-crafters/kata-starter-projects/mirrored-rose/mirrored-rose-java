package inventory;

/**
 * @author Danil Suits (danil@vast.com)
 */
public class ItemGenerator {
    public static ItemGenerator named(String name, Range quality) {
        return new ItemGenerator(name, quality);
    }

    public static ItemGenerator named(String name) {
        return named(
                name,
                DEFAULT_QUALITY_RANGE
        );
    }

    static final Range DEFAULT_QUALITY_RANGE = Range.inclusive(0, 50);

    public final String name;
    final Range quality;

    final static Range SELL_IN = Range.inclusive(-10, 100);

    public ItemGenerator(String name, Range quality) {
        this.name = name;
        this.quality = quality;
    }

    public int sellIn(int seed) {
        return SELL_IN.crop(seed);
    }

    public int quality(int seed) {
        return quality.crop(seed);
    }
}
