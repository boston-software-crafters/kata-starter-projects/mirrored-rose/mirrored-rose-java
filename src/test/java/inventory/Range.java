package inventory;

/**
 * Helper class for managing test values.
 */
public class Range {
    final int min;
    final int width;

    public static Range inclusive(int min, int max) {
        int width = 1 + max - min;
        return new Range(min, width);
    }

    Range(int min, int width) {
        this.min = min;
        this.width = width;
    }

    public int crop(int seed) {
        return min + Math.abs(seed % width);
    }
}
