package release.v1;

import inventory.Name;
import inventory.UpdateSingleItem;
import legacy.Item;

class ExampleTests {
    static void run() {
        // When you are ready to move on to the next test, simply uncomment the line
        // The first test is uncommented to get you started.
        test(Name.VEST, 10, 20, 9, 19);
        test(Name.ELIXIR, 10, 1, 9, 0);
        test(Name.VEST, 0, 20, -1, 19);
        test(Name.ELIXIR,  10, 0, 9, -1);
        test(Name.ELIXIR,  0, 0, -1, -1);
        test(Name.BRIE, 10, 20, 9, 19);
        test(Name.BRIE, -2, 43, -3, 42);
        test(Name.BRIE, 10, 47, 9, 46);
        test(Name.BRIE, 10, 48, 9, 47);
        test(Name.BRIE, 10, 49, 9, 48);
        test(Name.BRIE, 10, 50, 9, 49);
        test(Name.BRIE, -2, 47, -3, 46);
        test(Name.BRIE, -2, 48, -3, 47);
        test(Name.BRIE, -2, 49, -3, 48);
        test(Name.BRIE, -2, 50, -3, 49);
        test(Name.SULFURAS, 10, 20, 9, 19);
        test(Name.SULFURAS, 10, 50, 9, 49);
        test(Name.SULFURAS, 10, 100, 9, 99);
        test(Name.TICKETS, 15, 20, 14, 19);
        test(Name.TICKETS, 15, 50, 14, 49);
        test(Name.TICKETS, 10, 20, 9, 19);
        test(Name.TICKETS, 5, 20, 4, 19);
        test(Name.TICKETS, 0, 20, -1, 19);
        test(Name.CAKE, 15, 20, 14, 19);
        test(Name.CAKE, 0, 20, -1, 19);
    }

    public static void test(String name, int beforeSellIn, int beforeQuality,
                            int afterSellIn, int afterQuality) {
        Item control = new Item(name, afterSellIn, afterQuality);
        String expected = control.toString();
        String actual = scenario(name, beforeSellIn, beforeQuality);

        if ( ! expected.equals(actual)) {
            String scenario = new Item(name, beforeSellIn, beforeQuality).toString();
            throw new AssertionError(
                    "\t" + "Scenario: " + scenario
                            + "\n\t" + "Expected: " + expected
                            + "\n\t" + "  Actual: " + actual
            );
        }
    }

    public static String scenario(String name, int sellIn, int quality) {
        return UpdateSingleItem.scenario(name,sellIn,quality);
    }

    public static void main(String[] args) {
        try {
            run();
            System.out.println("SUCCESS");
        } catch (AssertionError e) {
            System.out.println(e.getMessage());
            System.out.println("FAILED");
        }
    }
}
