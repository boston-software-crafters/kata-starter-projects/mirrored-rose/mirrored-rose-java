package release.v4;

import inventory.ItemGenerator;
import inventory.Name;
import inventory.Range;
import legacy.Item;
import legacy.MirroredRose;

class AcceptanceTests {
    static void scenario(Item ... items) {
        MirroredRose sut = new MirroredRose(items);
        sut.updateQuality();
    }

    static final ItemGenerator[] GENERATORS = {
            ItemGenerator.named(Name.VEST),
            ItemGenerator.named(Name.ELIXIR),
            ItemGenerator.named(Name.BRIE),
            ItemGenerator.named(Name.TICKETS),
            ItemGenerator.named(Name.CAKE),
            ItemGenerator.named(
                    Name.SULFURAS
            )
    };

    static void run() {
        final int ARBITRARY_BIG = 200;

        for (ItemGenerator generator : GENERATORS) {
            for (int sellInSeed = 0; sellInSeed < ARBITRARY_BIG; ++sellInSeed) {
                for (int qualitySeed = 0; qualitySeed < ARBITRARY_BIG; ++qualitySeed) {
                    test(
                            generator,
                            sellInSeed,
                            qualitySeed
                    )                  ;
                }
            }
        }
    }

    static void test(
            ItemGenerator generator,
            int sellInSeed,
            int qualitySeed
    ) {
        int sellIn = generator.sellIn(sellInSeed);
        int quality = generator.quality(qualitySeed);

        test(
                generator.name,
                sellIn,
                quality
        );
    }


    static void test(
            String name,
            int sellIn,
            int quality
    ) {
        Item control = new Item(name, sellIn, quality);
        ReferenceImplementation.updateQuality(control);

        Item experiment = new Item(name, sellIn, quality);
        scenario(experiment);

        if (notMatched(control, experiment)) {
            Item scenario = new Item(name, sellIn, quality);
            throw new AssertionError(
                    "Oracle failed:"
                            + "\n\tScenario: " + scenario.toString()
                            + "\n\tExpected: " + control.toString()
                            + "\n\t  Actual: " + experiment.toString()
            );
        }
    }

    static boolean notMatched(Item expected, Item actual) {
        return notMatched(
                expected.toString(),
                actual.toString()
        );
    }

    static boolean notMatched(String expected, String actual) {
        return ! expected.equals(actual);
    }

    public static void main(String[] args) {
        try {
            run();
            System.out.println("SUCCESS");
        } catch (AssertionError e) {
            System.out.println(e.getMessage());
            System.out.println("FAILED");
        }
    }
}
